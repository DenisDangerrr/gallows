// Создаём массив слов для игры
let words = [
    "программа",
    "макака",
    "прекрасный",
    "оладушек",
    "меню",
    "школа",
    "надежный",
    "удивительный",
    "выбирать",
    "болеть",
    "стадион"
];
let word = words[Math.floor(Math.random() * words.length)]; // выбираем слово

let answerArray = []; //пустой массив для ответов

for (let i = 0; i < word.length; i++) {
    answerArray[i] = "_";
} //показываем сколько пустых мест(букв) в слове для игрока

let remainingLetters = word.length; // Счётчик букв

while (remainingLetters > 0){ //цикл игры
    //Показываем состояние игры
    alert(answerArray.join(" "));
    //запрашиваем вариант ответа
    let guess = prompt("Угадайте букву или нажмите Отмена для выхода из игры.").toLowerCase();

    //проверяем на корректность ввода
     if (guess === null) {
         // Выходим из игрового цикла, если нажали отмену
            break;
     }
     else if (guess.length !== 1) {
            alert("Пожалуйста, введите только одну букву.");
        }
     else {    // обновление состояния игры (добавление правильных букв)
         for (let j = 0; j < word.length; j++) {
         if (word[j] === guess) {
                 answerArray[j] = guess;
                 remainingLetters--;
             }
         }
     }
}

alert(answerArray.join(" "));
alert("Отлично! Было загадано слово " + word);

